package Class::Docker::Network;

use 5.18.0;
use strict;
use warnings;
use Class::Docker::Network::Container;

use parent qw/Class::Docker::Abstract::Named Class::Docker::Abstract::Identified/;

=head1 NAME

Class::Docker::Container2Command - The great new Class::Docker::Container2Command!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = {@_};
    if ( scalar keys %{ $self->{Options} } > 0 ) {
        require Class::Docker::Network::Options;
        $self->{Options} = Class::Docker::Network::Options->new( %{ $self->{Options} } );
    }
    if ( scalar keys %{ $self->{IPAM} } > 0 ) {
        require Class::Docker::Network::IPAddressManagement;
        $self->{IPAM} = Class::Docker::Network::IPAddressManagement->new( %{ $self->{IPAM} } );
    }
    $self->{Containers}->{$_} = Class::Docker::Network::Container->new( %{ $self->{Containers}->{$_} } )
      for keys %{ $self->{Containers} };
    bless $self, $class;
    return $self;
}

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Class::Docker::Container;

    my $foo = Class::Docker::Container->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 aliases

Getter/Setter for internal hash key Aliases.

=cut

sub aliases {
    return $_[0]->{Aliases} unless $_[1];
    $_[0]->{Aliases} = $_[1];
    return $_[0]->{Aliases};
}

=head2 driver

Getter/Setter for internal hash key Driver.

=cut

sub driver {
    return $_[0]->{Driver} unless $_[1];
    $_[0]->{Driver} = $_[1];
    return $_[0]->{Driver};
}

=head2 gateway

Getter/Setter for internal hash key Gateway.

=cut

sub gateway {
    return $_[0]->{Gateway} unless $_[1];
    $_[0]->{Gateway} = $_[1];
    return $_[0]->{Gateway};
}

=head2 global_ip_v6

Getter/Setter for internal hash key GlobalIPv6Address.

=cut

sub global_ip_v6 {
    return $_[0]->{GlobalIPv6Address} unless $_[1];
    $_[0]->{GlobalIPv6Address} = $_[1];
    return $_[0]->{GlobalIPv6Address};
}

=head2 ip_address

Getter/Setter for internal hash key IPAddress.

=cut

sub ip_address {
    return $_[0]->{IPAddress} unless $_[1];
    $_[0]->{IPAddress} = $_[1];
    return $_[0]->{IPAddress};
}

=head2 ip_am_config

Getter/Setter for internal hash key IPAMConfig.

=cut

sub ip_am_config {
    return $_[0]->{IPAMConfig} unless $_[1];
    $_[0]->{IPAMConfig} = $_[1];
    return $_[0]->{IPAMConfig};
}

=head2 ip_prefixLen

Getter/Setter for internal hash key IPPrefixLen.

=cut

sub ip_prefixLen {
    return $_[0]->{IPPrefixLen} unless $_[1];
    $_[0]->{IPPrefixLen} = $_[1];
    return $_[0]->{IPPrefixLen};
}

=head2 ip_v6_gateway

Getter/Setter for internal hash key IPv6Gateway.

=cut

sub ip_v6_gateway {
    return $_[0]->{IPv6Gateway} unless $_[1];
    $_[0]->{IPv6Gateway} = $_[1];
    return $_[0]->{IPv6Gateway};
}

=head2 ipam

Getter/Setter for internal hash key IPAM.

=cut

sub ipam {
    return $_[0]->{IPAM} unless $_[1];
    $_[0]->{IPAM} = $_[1];
    return $_[0]->{IPAM};
}

=head2 links

Getter/Setter for internal hash key Links.

=cut

sub links {
    return $_[0]->{Links} unless $_[1];
    $_[0]->{Links} = $_[1];
    return $_[0]->{Links};
}

=head2 mac_address

Getter/Setter for internal hash key MacAddress.

=cut

sub mac_address {
    return $_[0]->{MacAddress} unless $_[1];
    $_[0]->{MacAddress} = $_[1];
    return $_[0]->{MacAddress};
}

=head2 scope

Getter/Setter for internal hash key Scope.

=cut

sub scope {
    return $_[0]->{Scope} unless $_[1];
    $_[0]->{Scope} = $_[1];
    return $_[0]->{Scope};
}

1;    # End of Class::Docker::Container2Command

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker-Container>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Class::Docker::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker-Container>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker-Container>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker-Container>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker-Container/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut

1;    # End of Class::Docker::Container
