package Class::Docker::Image;

use 5.18.0;
use strict;
use warnings;

use parent qw/Class::Docker::Abstract::Named Class::Docker::Abstract::Identified/;

use Class::Docker::GraphDriver;
use Class::Docker::Image::Config;
use Class::Docker::Image::RootFS;

=head1 NAME

Class::Docker::Image - The great new Class::Docker::Image!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = {@_};
    $self->{Config} = Class::Docker::Image::Config->new( %{ $self->{Config} } )
      if $self->{Config};
    $self->{GraphDriver} =
      Class::Docker::GraphDriver->new( %{ $self->{GraphDriver} } )
      if $self->{GraphDriver};
    $self->{RootFS} = Class::Docker::Image::RootFS->new( %{ $self->{RootFS} } )
      if $self->{RootFS};
    bless $self, $class;
    return $self;
}

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Class::Docker::Container;

    my $foo = Class::Docker::Container->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 config

Getter/Setter for internal hash key Config.

=cut

sub config {
    return $_[0]->{Config} unless $_[1];
    $_[0]->{Config} = $_[1];
    return $_[0]->{Config};
}

=head2 tags

Getter/Setter for internal hash key tags.

=cut

sub tags {
    return $_[0]->{RepoTags} unless $_[1];
    $_[0]->{RepoTags} = $_[1];
    return $_[0]->{RepoTags};
}

=head2 architecture

Getter/Setter for internal hash key Architecture.

=cut

sub architecture {
    return $_[0]->{Architecture} unless $_[1];
    $_[0]->{Architecture} = $_[1];
    return $_[0]->{Architecture};
}

=head2 author

Getter/Setter for internal hash key Author.

=cut

sub author {
    return $_[0]->{Author} unless $_[1];
    $_[0]->{Author} = $_[1];
    return $_[0]->{Author};
}

=head2 docker_version

Getter/Setter for internal hash key DockerVersion.

=cut

sub docker_version {
    return $_[0]->{DockerVersion} unless $_[1];
    $_[0]->{DockerVersion} = $_[1];
    return $_[0]->{DockerVersion};
}

=head2 created

Getter/Setter for internal hash key Created.

=cut

sub created {
    return $_[0]->{Created} unless $_[1];
    $_[0]->{Created} = $_[1];
    return $_[0]->{Created};
}

=head2 latest

Get latest version form image.

=cut

sub latest {
    return $_[0]->{RepoTags}->[-1];
}

=head2 os

Getter/Setter for internal hash key Os.

=cut

sub os {
    return $_[0]->{Os} unless $_[1];
    $_[0]->{Os} = $_[1];
    return $_[0]->{Os};
}

=head2 size

Getter/Setter for internal hash key Size.

=cut

sub size {
    return $_[0]->{Size} unless $_[1];
    $_[0]->{Size} = $_[1];
    return $_[0]->{Size};
}

=head2 virtual_size

Getter/Setter for internal hash key VirtualSize.

=cut

sub virtual_size {
    return $_[0]->{VirtualSize} unless $_[1];
    $_[0]->{VirtualSize} = $_[1];
    return $_[0]->{VirtualSize};
}

=head2 comment

Getter/Setter for internal hash key Comment.

=cut

sub comment {
    return $_[0]->{Comment} unless $_[1];
    $_[0]->{Comment} = $_[1];
    return $_[0]->{Comment};
}

=head2 repo_digests

Getter/Setter for internal hash key RepoDigests.

=cut

sub repo_digests {
    return $_[0]->{RepoDigests} unless $_[1];
    $_[0]->{RepoDigests} = $_[1];
    return $_[0]->{RepoDigests};
}

=head2 parent

Getter/Setter for internal hash key Parent.

=cut

sub parent {
    return $_[0]->{Parent} unless $_[1];
    $_[0]->{Parent} = $_[1];
    return $_[0]->{Parent};
}

=head2 root_fs

Getter/Setter for internal hash key RootFS.

=cut

sub root_fs {
    return $_[0]->{RootFS} unless $_[1];
    $_[0]->{RootFS} = $_[1];
    return $_[0]->{RootFS};
}

=head2 parent_id

Getter/Setter for internal hash key ParentId.

=cut

sub parent_id {
    return $_[0]->{ParentId} unless $_[1];
    $_[0]->{ParentId} = $_[1];
    return $_[0]->{ParentId};
}

=head2 children

Getter/Setter for internal hash key _children.
This hashkey is not given from the docker API.

=cut

sub children {
    return $_[0]->{_children} unless $_[1];
    $_[0]->{_children} = $_[1];
    return $_[0]->{_children};
}

=head2 untagged

Getter/Setter for internal hash key _untagged.
This hashkey is not given from the docker API.

=cut

sub untagged {
    return $_[0]->{_untagged} unless $_[1];
    $_[0]->{_untagged} = $_[1];
    return $_[0]->{_untagged};
}

=head2 history

Getter/Setter for internal hash key _history.
This hashkey is not given from the docker API.

=cut

sub history {
    return $_[0]->{_history} unless $_[1];
    $_[0]->{_history} = $_[1];
    return $_[0]->{_history};
}

=head2 creation_log

Getter/Setter for internal hash key _creation_log.
This hashkey is not given from the docker API.

=cut

sub creation_log {
    return $_[0]->{_creation_log} unless $_[1];
    $_[0]->{_creation_log} = $_[1];
    return $_[0]->{_creation_log};
}

1;    # End of Class::Docker::Image

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker-Container>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Class::Docker::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker-Container>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker-Container>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker-Container>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker-Container/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut

1;    # End of Class::Docker::Container
