package Class::Docker::Engine;

use 5.18.0;
use strict;
use warnings;

use parent 'Class::Docker::Abstract::Identified';

use Class::Docker::Swarm;
use Class::Docker::Platform;
use Class::Docker::Engine::RegistryConfig;
use Class::Docker::Engine::Plugin;

=head1 NAME

Class::Docker::Container2Command - The great new Class::Docker::Container2Command!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = {@_};
    $self->{Version}        = $self->{EngineVersion}                                                            if $self->{EngineVersion};
    $self->{Swarm}          = Class::Docker::Swarm->new( %{ delete $self->{Swarm} } )                           if $self->{Swarm};
    $self->{Platform}       = Class::Docker::Platform->new( %{ delete $self->{Platform} } )                     if $self->{Platform};
    $self->{RegistryConfig} = Class::Docker::Engine::RegistryConfig->new( %{ delete $self->{RegistryConfig} } ) if $self->{RegistryConfig};

    #$self->{Plugins} = [ map { Class::Docker::Engine::Plugin->new(%$_) } @{ $self->{Plugins} } ]
    #  if $self->{Plugins};

    bless $self, $class;
    return $self;
}

=head2 api_version

Getter/Setter for internal hash key ApiVersion.

=cut

sub api_version {
    return $_[0]->{ApiVersion} unless $_[1];
    $_[0]->{ApiVersion} = $_[1];
    return $_[0]->{ApiVersion};
}

=head2 arch

Getter/Setter for internal hash key Arch.

=cut

sub arch {
    return $_[0]->{Arch} unless $_[1];
    $_[0]->{Arch} = $_[1];
    return $_[0]->{Arch};
}

=head2 build_time

Getter/Setter for internal hash key BuildTime.

=cut

sub build_time {
    return $_[0]->{BuildTime} unless $_[1];
    $_[0]->{BuildTime} = $_[1];
    return $_[0]->{BuildTime};
}

=head2 git_commit

Getter/Setter for internal hash key GitCommit.

=cut

sub git_commit {
    return $_[0]->{GitCommit} unless $_[1];
    $_[0]->{GitCommit} = $_[1];
    return $_[0]->{GitCommit};
}

=head2 go_version

Getter/Setter for internal hash key GoVersion.

=cut

sub go_version {
    return $_[0]->{GoVersion} unless $_[1];
    $_[0]->{GoVersion} = $_[1];
    return $_[0]->{GoVersion};
}

=head2 kernel_version

Getter/Setter for internal hash key KernelVersion.

=cut

sub kernel_version {
    return $_[0]->{KernelVersion} unless $_[1];
    $_[0]->{KernelVersion} = $_[1];
    return $_[0]->{KernelVersion};
}

=head2 min_api_version

Getter/Setter for internal hash key MinAPIVersion.

=cut

sub min_api_version {
    return $_[0]->{MinAPIVersion} unless $_[1];
    $_[0]->{MinAPIVersion} = $_[1];
    return $_[0]->{MinAPIVersion};
}

=head2 os

Getter/Setter for internal hash key Os.

=cut

sub os {
    return $_[0]->{Os} unless $_[1];
    $_[0]->{Os} = $_[1];
    return $_[0]->{Os};
}

=head2 version

Getter/Setter for internal hash key Version.

=cut

sub version {
    return $_[0]->{Version} unless $_[1];
    $_[0]->{Version} = $_[1];
    return $_[0]->{Version};
}

=head2 platform

Getter/Setter for internal hash key Platform.

=cut

sub platform {
    return $_[0]->{Platform} unless $_[1];
    $_[0]->{Platform} = $_[1];
    return $_[0]->{Platform};
}

=head2 plugins

Getter/Setter for internal hash key Plugins.

=cut

sub plugins {
    return $_[0]->{Plugins} unless $_[1];
    $_[0]->{Plugins} = $_[1];
    return $_[0]->{Plugins};
}

=head2 hostname

Getter/Setter for internal hash key Hostname.

=cut

sub hostname {
    return $_[0]->{Hostname} unless $_[1];
    $_[0]->{Hostname} = $_[1];
    return $_[0]->{Hostname};
}

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Class::Docker::Container;

    my $foo = Class::Docker::Container->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=cut

1;    # End of Class::Docker::Container2Command

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker-Container>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Class::Docker::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker-Container>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker-Container>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker-Container>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker-Container/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut

1;    # End of Class::Docker::Container
