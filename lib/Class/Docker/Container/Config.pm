package Class::Docker::Container::Config;

use 5.18.0;
use strict;
use warnings;

=head1 NAME

Class::Docker::Config - The great new Class::Docker::Config!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = {@_};
    bless $self, $class;
    return $self;
}

=head1 SYNOPSIS

    use Class::Docker::Container::Config;

    my $config = Class::Docker::Container::Config->new();

    my $container = Class::Docker::Container->new(( config => {}));

    $container->config($config);

=head1 SUBROUTINES/METHODS

=head2 attach_stderr

Getter/Setter for internal hash key AttachStderr.

=cut

sub attach_stderr {
    return $_[0]->{AttachStderr} unless $_[1];
    $_[0]->{AttachStderr} = $_[1];
    return $_[0]->{AttachStderr};
}

=head2 attach_stdin

Getter/Setter for internal hash key AttachStdin.

=cut

sub attach_stdin {
    return $_[0]->{AttachStdin} unless $_[1];
    $_[0]->{AttachStdin} = $_[1];
    return $_[0]->{AttachStdin};
}

=head2 attach_stdout

Getter/Setter for internal hash key AttachStdout.

=cut

sub attach_stdout {
    return $_[0]->{AttachStdout} unless $_[1];
    $_[0]->{AttachStdout} = $_[1];
    return $_[0]->{AttachStdout};
}

=head2 cmd

Getter/Setter for internal hash key Cmd.

=cut

sub cmd {
    return $_[0]->{Cmd} unless $_[1];
    $_[0]->{Cmd} = $_[1];
    return $_[0]->{Cmd};
}

=head2 domainname

Getter/Setter for internal hash key Domainname.

=cut

sub domainname {
    return $_[0]->{Domainname} unless $_[1];
    $_[0]->{Domainname} = $_[1];
    return $_[0]->{Domainname};
}

=head2 entrypoint

Getter/Setter for internal hash key Entrypoint.

=cut

sub entrypoint {
    return $_[0]->{Entrypoint} unless $_[1];
    $_[0]->{Entrypoint} = $_[1];
    return $_[0]->{Entrypoint};
}

=head2 env

Getter/Setter for internal hash key Env.

=cut

sub env {
    return $_[0]->{Env} unless $_[1];
    $_[0]->{Env} = $_[1];
    return $_[0]->{Env};
}

=head2 hostname

Getter/Setter for internal hash key Hostname.

=cut

sub hostname {
    return $_[0]->{Hostname} unless $_[1];
    $_[0]->{Hostname} = $_[1];
    return $_[0]->{Hostname};
}

=head2 image

Getter/Setter for internal hash key Image.

=cut

sub image {
    return $_[0]->{Image} unless $_[1];
    $_[0]->{Image} = $_[1];
    return $_[0]->{Image};
}

=head2 on_build

Getter/Setter for internal hash key OnBuild.

=cut

sub on_build {
    return $_[0]->{OnBuild} unless $_[1];
    $_[0]->{OnBuild} = $_[1];
    return $_[0]->{OnBuild};
}

=head2 open_stdin

Getter/Setter for internal hash key OpenStdin.

=cut

sub open_stdin {
    return $_[0]->{OpenStdin} unless $_[1];
    $_[0]->{OpenStdin} = $_[1];
    return $_[0]->{OpenStdin};
}

=head2 open_once

Getter/Setter for internal hash key StdinOnce.

=cut

sub open_once {
    return $_[0]->{StdinOnce} unless $_[1];
    $_[0]->{StdinOnce} = $_[1];
    return $_[0]->{StdinOnce};
}

=head2 tty

Getter/Setter for internal hash key Tty.

=cut

sub tty {
    return $_[0]->{Tty} unless $_[1];
    $_[0]->{Tty} = $_[1];
    return $_[0]->{Tty};
}

=head2 user

Getter/Setter for internal hash key User.

=cut

sub user {
    return $_[0]->{User} unless $_[1];
    $_[0]->{User} = $_[1];
    return $_[0]->{User};
}

=head2 volumes

Getter/Setter for internal hash key Volumes.

=cut

sub volumes {
    return $_[0]->{Volumes} unless $_[1];
    $_[0]->{Volumes} = $_[1];
    return $_[0]->{Volumes};
}

=head2 working_dir

Getter/Setter for internal hash key WorkingDir.

=cut

sub working_dir {
    return $_[0]->{WorkingDir} unless $_[1];
    $_[0]->{WorkingDir} = $_[1];
    return $_[0]->{WorkingDir};
}

1;    # End of Class::Docker::Config

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker-Container>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Class::Docker::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker-Container>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker-Container>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker-Container>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker-Container/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut

1;    # End of Class::Docker::Container
