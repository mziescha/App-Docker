package Class::Docker::Container::HostConfig;

=head1 NAME

Class::Docker::Container::HostConfig - HostConfig class for Container

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

use 5.18.0;
use strict;
use warnings;
use Class::Docker::Port;
use Class::Docker::Container::RestartPolicy;

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = {@_};
    bless $self, $class;
    $self->{RestartPolicy} = Class::Docker::Container::RestartPolicy->new( %{ $self->{RestartPolicy} } ) if $self->{RestartPolicy};
    if ( $self->{PortBindings} ) {
        for my $key ( keys %{ $self->{PortBindings} } ) {
            my ( $port, $protocoll ) = $key =~ m~^(.*)/(.*)$~;
            $_ = Class::Docker::Port->new( %$_, Protocoll => $protocoll, PrivatePort => $port ) for @{ $self->{PortBindings}->{$key} };
        }
    }
    return $self;
}

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Class::Docker::Container::HostConfig;

    my %vars = (
        NetworkMode => '0',
        Binds => ,
        PortBindings => [Class::Docker::Port->new,Class::Docker::Port->new],
        AutoRemove => '123',
        RestartPolicy => Class::Docker::Container::RestartPolicy->new,
    );

    my $host_config = Class::Docker::Container::HostConfig->new();
    

=head1 SUBROUTINES/METHODS

=head2 network_mode

Getter/Setter for internal hash key NetworkMode.

=cut

sub network_mode {
    return $_[0]->{NetworkMode} unless $_[1];
    $_[0]->{NetworkMode} = $_[1];
    return $_[0]->{NetworkMode};
}

=head2 binds

Getter/Setter for internal hash key Binds.

=cut

sub binds {
    return $_[0]->{Binds} unless $_[1];
    $_[0]->{Binds} = $_[1];
    return $_[0]->{Binds};
}

=head2 port_bindings

Getter/Setter for internal hash key PortBindings.

=cut

sub port_bindings {
    return $_[0]->{PortBindings} unless $_[1];
    $_[0]->{PortBindings} = $_[1];
    return $_[0]->{PortBindings};
}

=head2 auto_remove

Getter/Setter for internal hash key AutoRemove.

=cut

sub auto_remove {
    return $_[0]->{AutoRemove} unless $_[1];
    $_[0]->{AutoRemove} = $_[1];
    return $_[0]->{AutoRemove};
}

=head2 restart_policy

Getter/Setter for internal hash key RestartPolicy.

=cut

sub restart_policy {
    return $_[0]->{RestartPolicy} unless $_[1];
    $_[0]->{RestartPolicy} = $_[1];
    return $_[0]->{RestartPolicy};
}

1;    # End of Class::Docker::Container::HostConfig

__END__

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.
=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Class::Docker::Container::HostConfig
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut
