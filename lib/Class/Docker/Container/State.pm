package Class::Docker::Container::State;

use 5.18.0;
use strict;
use warnings;

=head1 NAME

Class::Docker::Container2Command - The great new Class::Docker::Container2Command!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = {@_};
    bless $self, $class;
    return $self;
}

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Class::Docker::Container;

    my $foo = Class::Docker::Container->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 dead

Getter/Setter for internal hash key Dead.

=cut

sub dead {
    return $_[0]->{Dead} unless $_[1];
    $_[0]->{Dead} = $_[1];
    return $_[0]->{Dead};
}

=head2 error

Getter/Setter for internal hash key Error.

=cut

sub error {
    return $_[0]->{Error} unless $_[1];
    $_[0]->{Error} = $_[1];
    return $_[0]->{Error};
}

=head2 exit_code

Getter/Setter for internal hash key ExitCode.

=cut

sub exit_code {
    return $_[0]->{ExitCode} unless $_[1];
    $_[0]->{ExitCode} = $_[1];
    return $_[0]->{ExitCode};
}

=head2 finished_at

Getter/Setter for internal hash key FinishedAt.

=cut

sub finished_at {
    return $_[0]->{FinishedAt} unless $_[1];
    $_[0]->{FinishedAt} = $_[1];
    return $_[0]->{FinishedAt};
}

=head2 oom_killed

Getter/Setter for internal hash key OOMKilled.

=cut

sub oom_killed {
    return $_[0]->{OOMKilled} unless $_[1];
    $_[0]->{OOMKilled} = $_[1];
    return $_[0]->{OOMKilled};
}

=head2 paused

Getter/Setter for internal hash key Paused.

=cut

sub paused {
    return $_[0]->{Paused} unless $_[1];
    $_[0]->{Paused} = $_[1];
    return $_[0]->{Paused};
}

=head2 pid

Getter/Setter for internal hash key Pid.

=cut

sub pid {
    return $_[0]->{Pid} unless $_[1];
    $_[0]->{Pid} = $_[1];
    return $_[0]->{Pid};
}

=head2 restarting

Getter/Setter for internal hash key Restarting.

=cut

sub restarting {
    return $_[0]->{Restarting} unless $_[1];
    $_[0]->{Restarting} = $_[1];
    return $_[0]->{Restarting};
}

=head2 running

Getter/Setter for internal hash key Running.

=cut

sub running {
    return $_[0]->{Running} unless $_[1];
    $_[0]->{Running} = $_[1];
    return $_[0]->{Running};
}

=head2 started_at

Getter/Setter for internal hash key StartedAt.

=cut

sub started_at {
    return $_[0]->{StartedAt} unless $_[1];
    $_[0]->{StartedAt} = $_[1];
    return $_[0]->{StartedAt};
}

=head2 status

Getter/Setter for internal hash key Status.

=cut

sub status {
    return $_[0]->{Status} unless $_[1];
    $_[0]->{Status} = $_[1];
    return $_[0]->{Status};
}

1;    # End of Class::Docker::Container2Command

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker-Container>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Class::Docker::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker-Container>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker-Container>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker-Container>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker-Container/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut

1;    # End of Class::Docker::Container
