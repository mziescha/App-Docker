package Class::Docker::Node;

use 5.18.0;
use strict;
use warnings;

use parent qw/Class::Docker::Abstract::Identified/;

use DateTime::Format::Docker;
use Class::Docker::Version;
use Class::Docker::Node::ManagerStatus;
use Class::Docker::Node::Description;
use Class::Docker::Node::Spec;
use Class::Docker::Node::Status;

=head1 NAME

Class::Docker::Node - The great new Class::Docker::Node!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = {@_};
    $self->{CreatedAt} = DateTime::Format::Docker->parse_datetime( $self->{CreatedAt} )
      if $self->{CreatedAt};
    $self->{Version} = Class::Docker::Version->new( %{ $self->{Version} } )
      if $self->{Version};
    $self->{UpdatedAt} = DateTime::Format::Docker->parse_datetime( $self->{UpdatedAt} )
      if $self->{UpdatedAt};
    $self->{ManagerStatus} = Class::Docker::Node::ManagerStatus->new( %{ $self->{ManagerStatus} } )
      if $self->{ManagerStatus};
    $self->{Resources} = Class::Docker::Resources->new( $self->{Resources} )
      if $self->{Resources};
    $self->{Description} = Class::Docker::Node::Description->new( %{ $self->{Description} } )
      if $self->{Description};
    $self->{Spec}   = Class::Docker::Node::Spec->new( %{ $self->{Spec} } )     if $self->{Spec};
    $self->{Status} = Class::Docker::Node::Status->new( %{ $self->{Status} } ) if $self->{Status};

    bless $self, $class;
    return $self;
}

=head1 SUBROUTINES/METHODS

=cut

=head2 created_at

Getter/Setter for internal hash key CreatedAt.

=cut

sub created_at {
    return $_[0]->{CreatedAt} unless $_[1];
    $_[0]->{CreatedAt} = $_[1];
    return $_[0]->{CreatedAt};
}

=head2 updated_at

Getter/Setter for internal hash key UpdatedAt.

=cut

sub updated_at {
    return $_[0]->{UpdatedAt} unless $_[1];
    $_[0]->{UpdatedAt} = $_[1];
    return $_[0]->{UpdatedAt};
}

=head2 spec

Getter/Setter for internal hash key Spec.

=cut

sub spec {
    return $_[0]->{Spec} unless $_[1];
    $_[0]->{Spec} = $_[1];
    return $_[0]->{Spec};
}

=head2 index

Getter/Setter for internal hash key Index.

=cut

sub index {
    return $_[0]->{Index} unless $_[1];
    $_[0]->{Index} = $_[1];
    return $_[0]->{Index};
}

=head2 status

Getter/Setter for internal hash key Status.

=cut

sub status {
    return $_[0]->{Status} unless $_[1];
    $_[0]->{Status} = $_[1];
    return $_[0]->{Status};
}

=head2 description

Getter/Setter for internal hash key Description.

=cut

sub description {
    return $_[0]->{Description} unless $_[1];
    $_[0]->{Description} = $_[1];
    return $_[0]->{Description};
}

=head2 manager_status

Getter/Setter for internal hash key ManagerStatus.

=cut

sub manager_status {
    return $_[0]->{ManagerStatus} unless $_[1];
    $_[0]->{ManagerStatus} = $_[1];
    return $_[0]->{ManagerStatus};
}

1;    # End of Class::Docker::Node

=head1 SYNOPSIS

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker-Container>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Class::Docker::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker-Container>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker-Container>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker-Container>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker-Container/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut

1;    # End of Class::Docker::Container
