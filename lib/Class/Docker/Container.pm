package Class::Docker::Container;

use 5.18.0;
use strict;
use warnings;

use parent qw/Class::Docker::Abstract::Named Class::Docker::Abstract::Identified/;

use Ref::Util qw/is_hashref/;
use DateTime::Format::Docker;
use Class::Docker::GraphDriver;
use Class::Docker::Image;
use Class::Docker::Container::Mount;
use Class::Docker::Container::NetworkSettings;
use Class::Docker::Container::HostConfig;
use Class::Docker::Container::Config;
use Class::Docker::Container::State;

=head1 NAME

Class::Docker::Container2Command - The great new Class::Docker::Container2Command!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = {@_};
    $self->{Mounts} = [ map { Class::Docker::Container::Mount->new(%$_) } @{ $self->{Mounts} } ]
      if $self->{Mounts};
    $self->{NetworkSettings} = Class::Docker::Container::NetworkSettings->new( %{ $self->{NetworkSettings} } )
      if $self->{NetworkSettings};
    $self->{HostConfig} = Class::Docker::Container::HostConfig->new( %{ $self->{HostConfig} } )
      if $self->{HostConfig};
    $self->{Config} = Class::Docker::Container::Config->new( %{ $self->{Config} } )
      if $self->{Config};
    $self->{GraphDriver} = Class::Docker::GraphDriver->new( %{ $self->{GraphDriver} } )
      if $self->{GraphDriver};
    $self->{State} =
        is_hashref $self->{State}
      ? Class::Docker::Container::State->new( %{ $self->{State} } )
      : $self->{State}
      if $self->{State};
    $self->{Created} =
      $self->{Created} =~ /^(\d{10,})/
      ? DateTime->from_epoch( epoch => $self->{Created} )
      : DateTime::Format::Docker->parse_datetime( $self->{Created} )
      if $self->{Created};

    $self->{Image} = Class::Docker::Image->new( RepoTags => [ $self->{Image} ] ) if ( $self->{Image} );
    $self->{Name} = $self->{Names}->[0]
      if ( !$self->{Name} && scalar @{ $self->{Names} } > 0 );
    bless $self, $class;
    return $self;
}

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Class::Docker::Container;

    my $foo = Class::Docker::Container->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 names

Getter/Setter for internal hash key Names.

=cut

sub names {
    return $_[0]->{Names} unless $_[1];
    $_[0]->{Names} = $_[1];
    return $_[0]->{Names};
}

=head2 created

Getter/Setter for internal hash key Created.

=cut

sub created {
    return $_[0]->{Created} unless $_[1];
    $_[0]->{Created} = $_[1];
    return $_[0]->{Created};
}

=head2 image

Getter/Setter for internal hash key Image.

=cut

sub image {
    return $_[0]->{Image} unless $_[1];
    $_[0]->{Image} = $_[1];
    return $_[0]->{Image};
}

=head2 image_id

Getter/Setter for internal hash key ImageID.

=cut

sub image_id {
    return $_[0]->{ImageID} unless $_[1];
    $_[0]->{ImageID} = $_[1];
    return $_[0]->{ImageID};
}

=head2 mounts

Getter/Setter for internal hash key Mounts.

=cut

sub mounts {
    return $_[0]->{Mounts} unless $_[1];
    $_[0]->{Mounts} = $_[1];
    return $_[0]->{Mounts};
}

=head2 network_settings

Getter/Setter for internal hash key NetworkSettings.

=cut

sub network_settings {
    return $_[0]->{NetworkSettings} unless $_[1];
    $_[0]->{NetworkSettings} = $_[1];
    return $_[0]->{NetworkSettings};
}

=head2 host_config

Getter/Setter for internal hash key HostConfig.

=cut

sub host_config {
    return $_[0]->{HostConfig} unless $_[1];
    $_[0]->{HostConfig} = $_[1];
    return $_[0]->{HostConfig};
}

=head2 args

Getter/Setter for internal hash key Args.

=cut

sub args {
    return $_[0]->{Args} unless $_[1];
    $_[0]->{Args} = $_[1];
    return $_[0]->{Args};
}

=head2 state

Getter/Setter for internal hash key State.

=cut

sub state {
    return $_[0]->{State} unless $_[1];
    $_[0]->{State} = $_[1];
    return $_[0]->{State};
}

=head2 config

Getter/Setter for internal hash key Config.

=cut

sub config {
    return $_[0]->{Config} unless $_[1];
    $_[0]->{Config} = $_[1];
    return $_[0]->{Config};
}

=head2 path

Getter/Setter for internal hash key Path.

=cut

sub path {
    return $_[0]->{Path} unless $_[1];
    $_[0]->{Path} = $_[1];
    return $_[0]->{Path};
}

=head2 privileged

Getter/Setter for internal hash key Privileged.

=cut

sub privileged {
    return $_[0]->{Privileged} unless $_[1];
    $_[0]->{Privileged} = $_[1];
    return $_[0]->{Privileged};
}

=head2 finished_at

Getter/Setter for internal hash key FinishedAt.

=cut

sub finished_at {
    return $_[0]->{FinishedAt} unless $_[1];
    $_[0]->{FinishedAt} = $_[1];
    return $_[0]->{FinishedAt};
}

=head2 started_at

Getter/Setter for internal hash key StartedAt.

=cut

sub started_at {
    return $_[0]->{StartedAt} unless $_[1];
    $_[0]->{StartedAt} = $_[1];
    return $_[0]->{StartedAt};
}

=head2 volumes_from

Getter/Setter for internal hash key VolumesFrom.

=cut

sub volumes_from {
    return $_[0]->{VolumesFrom} unless $_[1];
    $_[0]->{VolumesFrom} = $_[1];
    return $_[0]->{VolumesFrom};
}

1;    # End of Class::Docker::Container2Command

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker-Container>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Class::Docker::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker-Container>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker-Container>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker-Container>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker-Container/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut

1;    # End of Class::Docker::Container
