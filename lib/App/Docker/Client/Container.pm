package App::Docker::Client::Container;

use 5.18.0;
use strict;
use warnings;
use Ref::Util qw/is_arrayref/;
use Class::Docker::Container;

use parent 'App::Docker::Client';

=head1 NAME

App::Docker::Client::Container - The great new App::Docker::Client::Container!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    bless $self, $class;
    return $self;
}

=head1 SUBROUTINES/METHODS

=head2 remove

Remove container dy name or id

=cut

sub remove {
    my ( $self, $container, $query_params ) = @_;
    my $name = $container->id ? $container->id : $container->name =~ s/^\///r;
    return $self->delete( qq~/containers/$name~, $query_params );
}

=head2 start

=cut

sub start {
    my ( $self, $container, $options, $cb_params ) = @_;
    return $self->post( '/containers/' . $container->id . '/start', {}, { Detach => "true" } )
      if $options->{detach};

    return $self->attach( '/containers/' . $container->id . '/attach',
        {}, $cb_params->{std_in}, $cb_params->{std_out} );

    #JSON parameters:
    #Tty - Boolean value to allocate a pseudo-TTY.
    #{
    # "Detach": false,
    # "Tty": false,
    #}
}

=head2 stop

Stop the container id

=cut

sub stop {
    my ( $self, $container, $options ) = @_;
    $self->post( '/containers/' . $container->id . '/stop',
        { ( $options->{time} ? ( t => $options->{time} ) : () ), } );

    #JSON parameters:
    #Detach - Detach from the exec command.
    #Tty - Boolean value to allocate a pseudo-TTY.
    #{
    # "Detach": false,
    # "Tty": false,
    #}
}

=head2 create

=cut

sub create {
    my ( $self, $obj_container ) = @_;
    require App::Docker::Client::Converter::Container;
    require App::Docker::Client::Image;
    my $obj_image_client = App::Docker::Client::Image->new(%$self);
    my $obj_image        = $obj_image_client->inspect( $obj_container->image->latest );
    if ( $obj_image->isa('App::Docker::Client::Exception') ) {
        $obj_image = $obj_image_client->pull( $obj_container->image ) if ( $obj_image->is_not_fround );
    }
    my $container_info = $self->post(
        '/containers/create',
        { name => $obj_container->name },
        App::Docker::Client::Converter::Container::object2create($obj_container)
    );
    return $self->inspect( $obj_container->name );
}

=head2 inspect

=cut

sub inspect {
    my ( $self, $i_container_id ) = @_;
    return unless $i_container_id;
    my $container = $self->get(qq~/containers/$i_container_id/json~);
    require Class::Docker::Container;
    return Class::Docker::Container->new(%$container);
}

=head2 list

Returns list of Container objects

=cut

sub list {
    my ( $self, $hr_query_params ) = @_;
    my $item_info = $self->get( 'containers/json', $hr_query_params );
    my $containers = [];
    foreach my $item ( is_arrayref $item_info ? @$item_info : @{ [$item_info] } ) {
        push( @$containers, Class::Docker::Container->new(%$item) );
    }
    return $containers;
}

=head2 logs

=cut

sub logs {
    my ( $self, $obj_container, $hr_query_params, $hr_callback_io ) = @_;
    $self->get_stream_lwp(
        '/containers/' . $obj_container->id . '/logs',
        $hr_query_params,
        $hr_callback_io->{std_in},
        $hr_callback_io->{std_out}
    );
}

=head2 exec

=cut

sub exec {
    my ( $self, $obj_container, $hr_query_params, $hr_callback_io ) = @_;
    my $condvar = $self->post_stream_anyevent(
        '/containers/' . $obj_container->id . '/exec',
        $hr_query_params,
        $hr_callback_io->{std_in},
        $hr_callback_io->{std_out}
    );
    use DDP;
    p $self;
    $condvar->recv;
}

=head2 by_id

Get containers as hashref while key is image id

=cut

sub by_id {
    my ( $self, $hr_query_params ) = @_;
    return { map { $_->id => $_ } @{ $self->list($hr_query_params) } };
}

1;    # End of App::Docker::Client::Container

__END__

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::Docker::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut

1;    # End of App::Docker::Container
