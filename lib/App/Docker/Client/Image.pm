package App::Docker::Client::Image;

use 5.18.0;
use strict;
use warnings;

use parent 'App::Docker::Client';

=head1 NAME

App::Docker::Image - The great new App::Docker::Image!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    bless $self, $class;
    return $self;
}

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use App::Docker::Client::Image;

    my $client = App::Docker::Client::Image->new();
    ...

=head1 SUBROUTINES/METHODS

=head2 build 

Builds a docker image with following params

    {
        t => image/tag
    },
    {
        dockerfile => /path/to/Dockerfile
        receipt => "FROM ..."
    },
    {
        content_type => 'application/tar'
    }

=cut

sub build {
    my ( $self, $query, $body, $options, $callback ) = @_;
    require App::Docker::Client::Converter::Dockerfile;
    $self->post( '/build', $query,
        App::Docker::Client::Converter::Dockerfile::dockerfile2tar( $body->{dockerfile} )->write(),
        $options, $callback );
    my $image = $self->inspect( $query->{t} );

    #$image->creation_log($response) if ( defined $image );
    return $image;
}

=head2 remove

    Remove image by name or id

=cut

sub remove {
    my ( $self, $image_id ) = @_;
    my $image = $self->inspect($image_id);
    $image->untagged( $self->delete(qq~/images/$image_id~) );
    return $image;
}

=head2 history

=cut

sub history {
    my ( $self, $image ) = @_;
    require Class::Docker::Image;
    return [ map { Class::Docker::Image->new(%$_) } @{ $self->get( '/images/' . $image->id() . '/history' ) } ];
}

=head2 inspect

=cut

sub inspect {
    my ( $self, $image ) = @_;
    my $info = $self->get(qq~/images/$image/json~);
    require Scalar::Util;
    return $info if ( Scalar::Util::blessed($info) && $info->isa('App::Docker::Client::Exception') );
    require Class::Docker::Image;
    return Class::Docker::Image->new(%$info);
}

=head2 list

Returns list of image objects

=cut

sub list {
    my ( $self, $query, $options ) = @_;
    require Class::Docker::Image;
    return [ map { Class::Docker::Image->new(%$_) } @{ $self->get( '/images/json', $query, $options ) } ];
}

=head2 list_all_versions

Returns list of image objects

=cut

sub list_all_versions {
    my ( $self, $tag ) = @_;
    my $re = qr/^$tag/;
    require List::Util;
    return [
        grep defined,
        map {
            List::Util::first { /^${re}:*/ } @{ $_->tags || [] }
              ? @{ $_->tags }
              : ()
        } @{ $self->list() }
    ];
}

=head2 by_id

Get images as hashref while key is image id

=cut

sub by_id {
    my ( $self, $options ) = @_;
    return { map { $_->id => $_ } @{ $self->list($options) } };
}

=head2 tree

Get images as hashref while key is image id

=cut

sub tree {
    my ($self) = @_;
    my $images = $self->by_id( { all => 'true' } );
    my $tree = [ map { !$_->parent_id ? $_ : () } values %$images ];
    _add_childs( $_, $images ) for @$tree;
    return $tree;
}

=head2 pull

Pull image from registry?

=cut

sub pull {
    my ( $self, $image, $callback ) = @_;
    my ( $name, $tag ) = split ':', $image->latest;
    return $self->post( qq~/images/create~, { fromImage => $name, tag => $tag || 'latest' }, {}, undef, $callback );
}

=head2 _add_childs

=cut

sub _add_childs {
    my ( $image, $images ) = @_;
    $image->children( [ map { $_->parent_id eq $image->id ? _add_childs( $_, $images ) : () } values %$images ] );
    return $image;
}

1;    # End of App::Docker::Image

__END__

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::Docker::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut

1;    # End of App::Docker::Container
