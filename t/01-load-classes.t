#!perl -T
use 5.18.0;
use strict;
use warnings;
use Test::More;

plan tests => 39;

BEGIN {
    use_ok('Class::Docker::Container')                            || print "Bail out!\n";
    use_ok('Class::Docker::Node')                                 || print "Bail out!\n";
    use_ok('Class::Docker::Image')                                || print "Bail out!\n";
    use_ok('Class::Docker::Swarm')                                || print "Bail out!\n";
    use_ok('Class::Docker::Engine')                               || print "Bail out!\n";
    use_ok('Class::Docker::Platform')                             || print "Bail out!\n";
    use_ok('Class::Docker::Version')                              || print "Bail out!\n";
    use_ok('Class::Docker::GraphDriver')                          || print "Bail out!\n";
    use_ok('Class::Docker::Port')                                 || print "Bail out!\n";
    use_ok('Class::Docker::Resources')                            || print "Bail out!\n";
    use_ok('Class::Docker::Volume')                               || print "Bail out!\n";
    use_ok('Class::Docker::Network')                              || print "Bail out!\n";
    use_ok('Class::Docker::Node::Status')                         || print "Bail out!\n";
    use_ok('Class::Docker::Node::ManagerStatus')                  || print "Bail out!\n";
    use_ok('Class::Docker::Node::Spec')                           || print "Bail out!\n";
    use_ok('Class::Docker::Node::Description')                    || print "Bail out!\n";
    use_ok('Class::Docker::Engine::RegistryConfig')               || print "Bail out!\n";
    use_ok('Class::Docker::Engine::Plugin')                       || print "Bail out!\n";
    use_ok('Class::Docker::Abstract::Identified')                 || print "Bail out!\n";
    use_ok('Class::Docker::Abstract::Named')                      || print "Bail out!\n";
    use_ok('Class::Docker::Swarm::EncryptionConfig')              || print "Bail out!\n";
    use_ok('Class::Docker::Swarm::Orchestration')                 || print "Bail out!\n";
    use_ok('Class::Docker::Swarm::CAConfig')                      || print "Bail out!\n";
    use_ok('Class::Docker::Swarm::JoinTokens')                    || print "Bail out!\n";
    use_ok('Class::Docker::Swarm::Spec')                          || print "Bail out!\n";
    use_ok('Class::Docker::Swarm::Dispatcher')                    || print "Bail out!\n";
    use_ok('Class::Docker::Swarm::Raft')                          || print "Bail out!\n";
    use_ok('Class::Docker::Image::Config')                        || print "Bail out!\n";
    use_ok('Class::Docker::Image::RootFS')                        || print "Bail out!\n";
    use_ok('Class::Docker::Container::NetworkSettings')           || print "Bail out!\n";
    use_ok('Class::Docker::Container::State')                     || print "Bail out!\n";
    use_ok('Class::Docker::Container::Config')                    || print "Bail out!\n";
    use_ok('Class::Docker::Container::RestartPolicy')             || print "Bail out!\n";
    use_ok('Class::Docker::Container::Mount')                     || print "Bail out!\n";
    use_ok('Class::Docker::Container::HostConfig')                || print "Bail out!\n";
    use_ok('Class::Docker::Network::Container')                   || print "Bail out!\n";
    use_ok('Class::Docker::Network::Options')                     || print "Bail out!\n";
    use_ok('Class::Docker::Network::IPAddressManagement')         || print "Bail out!\n";
    use_ok('Class::Docker::Network::IPAddressManagement::Config') || print "Bail out!\n";

}

