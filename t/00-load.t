#!perl -T
use 5.18.0;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'App::Docker' ) || print "Bail out!\n";
}

diag( "Testing App::Docker $App::Docker::VERSION, Perl $], $^X" );
